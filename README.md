# What it is

This library provides command line parsing functions with several improvements over the POSIX getopt(3) call:

* command processing is done through callbacks rather than if-else,
* all data is kept in one structure,
* default callbacks for on/off flag, string, integer, float, and log parameters,
* built-in help and version renderer with word breaking,
* built-in error message display.

# Compiling

```
gcc -Wall -s -O2 -shared -fPIC -olibarguments.so arguments.c -lm
```

or you can use

```
gcc -DSYSLOG_NAMES=1 -Wall -s -O2 -shared -fPIC -olibarguments.so arguments.c -lm
```

if the `facilitynames` structure is defined in your `syslog.h`.
This provides syslog facility identifiers to be used with `argument_log`
tailored to your system.

# Installing

1. Copy the resulting *libarguments.so* to the chosen lib directory.
2. Copy arguments.h to the corresponding include directory.
3. Deploy manual pages.

# Using

Please refer to the manual pages provided.
