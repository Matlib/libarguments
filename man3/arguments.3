.Dd August 4, 2018
.Dt arguments 3
.Sh NAME
.Nm arguments
.Nd handling of command line parameters
.Sh SYNOPSIS
.In arguments.h
.Ft struct Argument Ar arguments [] = { ... };
.br
.Ft int Ar non_option =
.Fo arguments_parse
.Fa ...
.Fa argc
.Fa argv
.Fa arguments
.Fc
.Sh DESCRIPTION
The arguments library provides simple utility procedures for handling
.Em command line arguments 
(or options, parameters). It is designed to provide maximum simplicity to the programmer. The parser uses the callback model to handle each command line string. Five standard callback functions are included to provide the basic functionality (which should be enough for most purposes though).
.Pp
Features include:
.Bl -bullet
.It
support for UNIX-style short options 
.Fl ( o ) ,
.It
support for GNU-style long options
.Fl ( -option )
with parameters supplied after 
.Ql =
sign (GNU, 
.Fl -option=param )
or as another token (extension, 
.Fl -option Cm param ) ,
.It
built-in 
.Fl -version , -help
and
.Fl h,
.It
support for option parameters
.Fl ( xooutput , xo Cm output , Fl x -out=output
or
.Fl x -out Cm output ) ,
.It
support for non-option arguments (returned to the caller).
.El
.Ss "USAGE"
The array of recognized command line arguments is defined as follows:
.Bd -literal -offset 4m
struct Argument arguments [] = 
{
  { short , long , callback , flags , initial\-value, value\-label, help },
  { short , long , callback , flags , initial\-value, value\-label, help },
  ...
  { short , long , callback , flags , initial\-value, value\-label, help },
  { }
};
.Ed
.Pp
where:
.Bl -hang -offset 4m
.It Em short
.Vt ( char Va shrt )
- short option
.Ns ( Ux
style)
.It Em long
.Vt ( const char  * Ns Va lng )
- long option (GNU style)
.It Em callback
.Vt ( ArgumentCallback * Ns Va cb )
- callback function to be executed when the option is found in the command 
line, see section 
.Sx CALLBACKS
for more information
.It Em flags
.Vt ( int Va flags )
- see section
.Sx FLAGS
.It Em initial\-value
- where callback results are (usually) stored; since version 6 it is a
union of six types:
.Vt void * Ns Va value ,
.Vt unsigned long Va value_u ,
.Vt long Va value_int ,
.Vt const char * Ns Va value_str ,
.Vt char * Ns Va value_vs ,
and logging callback
.Va value_log
(see section
.Sx STANDARD CALLBACKS )
.It Em value\-label
.Vt ( char * )
- how the value name should shown on the help page; the default text is
.Ql PAR
.It Em help
.Vt ( char * )
- the argument's description on the help page.
.El
.Pp
An empty element ends the array (in fact the parser checks if
.Va arguments.cb
equals 
.Dv NULL ) .
Calling
.Xr arguments_parse 3
starts the process of examining command line arguments in order of their appearance, and executing associated callbacks:
.Bd -ragged -offset 4m
.Ft int 
.Fo arguments_parse 
.Fa "const char *program_name"
.Fa "const char *version"
.Fa "const char *usage"
.Fa "const char *short_description"
.Fa "const int argc"
.Fa "const char **argv"
.Fa "struct Argument *arguments"
.Fc
.Ed
.Pp
where
.Fa program_name , version , usage
and
.Fa short_description
of type
.Vt const char *
are informational strings that are rendered by the
.Fl -help
and
.Fl -version
options.
.Pp
The function returns index of the first 
.Em non-option argument 
(an argument not starting with 
.Ql - ) .
Contrary to the GNU
.Lb libc
implementation, this parser stops processing at first non-option 
argument (POSIX behaviour). The special option
.Fl -
ends all options and forces the parser function return index of the
following argument even if it starts with a
.Ql - .
.Pp
In case of an error (missing paramer, wrong number format, etc.) standard error message is printed and program exits with return code of
.Dv EXIT_FAILURE .
See section
.Sx HELP AND ERROR REPORTING
for more information.
.Pp
Typical use of the library:
.Bd -literal -offset 4m
#include <stdlib.h>
#include <stdio.h>
#include <arguments.h>

struct Argument arguments [] =
{
  {'v', "verbose", argument_bool, 0, {.value_u = 0}},
  {0,   "string",  argument_string, ARGUMENT_F_ONCE |
     ARGUMENT_F_PAR, {.value_str = "(default value)"}},
  {}
};

int main (const int argc, const char **argv)
{
  int p;
  p = arguments_parse ("example", "1.0", "[OTHER]", 
     "Demo", argc, argv, arguments);
  printf ("Verbosity level is: %ld; string = %s\\n",
     arguments [0].value_int, arguments [1].value_str);
  if (p < argc)
    printf ("First non-option argument: %s\\n", 
       argv [p]);
  return 0;
}
.Ed
.Pp
To compile this program you have to link against
.Lb libarchives ,
which is usually done by supplying
.Fl larchives
to the compiler.
.Pp
.Ss "FLAGS"
.Pa arguments.h
defines the following flags to be used within 
.Vt struct Arguments :
.Bl -hang -offset 4m
.It Dv ARGUMENT_F_OCC
marks the presence of an option in the command line; this flag is set by 
.Xr arguments_parse 3
and may be used to examine if the option was found among the arguments
.It Dv ARGUMENT_F_PAR
this option requires and accepts an additional parameter 
.Fl ( oparam , o Cm param
or
.Fl -option=param , -option Cm param )
.It Dv ARGUMENT_F_ONCE
this option 
.Em may 
appear only once in the command line
.It Dv ARGUMENT_F_OBL
this option 
.Em must 
appear at least once in the command line
.It Dv ARGUMENT_F_RET
protect the
.Dv ARGUMENT_F_OCC
flags when clearing the array with
.Xr arguments_clr_occurrence 3 .
.El
.Pp
By combining flags and callbacks the programmer gets specific behaviour. 
.Nm argument_bool
with no flags works like a verbose-style option (supplying more 
.Fl v Ap s
increases verbosity). Setting 
.Dv ARGUMENT_F_ONCE
makes it behave like a boolean option 
.Fl ( x
turns the feature on).
.Nm argument_int 
and 
.Nm argument_string
only make sense in conjunction with the
.Dv ARGUMENT_F_PAR
flag since they relay on a parameter stored in the value field.
.Ss "CALLBACKS"
The 
.Xr arguments 3
library uses callback model to perform tasks (no switch-case statements). The definition of a callback procedure is done with the macro:
.Bd -ragged -offset 4m
.Fn ARGUMENT_CALLBACK name
.Ed
.Pp
which expands to 
.Bd -ragged -offset 4m
.Ft void Fn name ...
.Ed
.Pp
The programmer should use this macro to define callbacks since the exact number of parameters and their order may change in the future. The following variables are available within the function:
.Bl -hang -offset 4m
.It Fa argc
.Vt ( const int )
and
.It Fa argv
.Vt ( const char ** )
what has been passed to 
.Xr arguments_parse 3 ,
.It Fa arg
.Vt ( struct Argument * )
- pointer to the argument which corresponds to the option that triggered the callback; it points to that argument only, not the whole array,
.It Fa parameter
.Vt ( const char * )
- string that represents this 
.Ns option Ap s
parameter if the flag
.Dv ARGUMENT_F_PAR
(see
.Sx FLAGS )
is set. Otherwise it equals
.Dv NULL .
.El
.Pp
Example definition of a callback:
.Bd -literal -offset 4m
ARGUMENT_CALLBACK (my_callback)
{
  assume (parameter != NULL);
  if (strcmp (parameter, "high") == 0)
    arg -> value_u = 1
  else if (strcmp (parameter, "low") != 0)
    arguments_error (arg, 
       "accepts only \\"high\\" or \\"low\\"");
}
.Ed
.Ss "STANDARD CALLBACKS"
The library defines a few standard callbacks for handling typical cases.
There is one callback that does not expect a parameter:
.Bl -bullet
.It
.Fn argument_bool
simply increases
.Va .value_u
by 1. With the
.Dv ARGUMENT_F_ONCE
flag it becomes a boolean type option.
.El
.Pp
These callbacks require an additional text to be given in the command line,
and need the
.Dv ARGUMENT_F_PAR
flag to be set:
.Bl -bullet
.It
.Fn argument_string
sets
.Va .value_str
to the supplied parameter.
.It
.Fn argument_vs
(variable string) is similar to
.Fn argument_string
except that it creates a local copy of the string with
.Xr strdup 3 .
.Va .value_vs
should be accessed with this callback.
.It 
.Fn argument_int
converts the parameter to a signed number of type
.Vt long
and stores it in
.Va .value_int .
.It 
.Fn argument_u
parses the parameter as an unsigned number of type
.Vt unsigned long
which is then accessible from
.Va .value_u .
.It
.Fn argument_fp
parses the parameter as a floating point number which is accessible from the
.Va .value_fp
field. Its precision depends on word length of the target architecture. It is
.Vt float
on 32-bit and
.Vt double
on 64-bit machines.
.It
.Fn argument_syslog
accepts
.Xr syslog 3
facility name and sets
.Va .value_int
accordingly.
.It
.Fn argument_log
accepts one of the following values as parameter: a case insensitive
.Xr syslog 3
facility name,
.Va stdout
or
.Va stderr
for writing to the terminal, or a direct file name where logging data
will be appended to. Argument's
.Va value_log
will be initialized with an appropriate function which can be either
.Xr vsyslog 3
or
.Fn log_callback_file ,
with file output being written to standard error by default.
.El
.Pp
The function stored in
.Va value_log
is called as:
.Bd -offset 4n
.Ft void
.Fo value_log
.Fa priority
.Fa format
.Fa va_list
.Fc
.Ed
.Pp
where
.Ft int Fa priority
is
.Xr syslog 3
priority code.
.Ss "HELP AND ERROR REPORTING"
The library supports the standard
.Fl -help
.Fl ( h )
and
.Fl -version
options which are handled internally using the information supplied to
.Fn arguments_parse .
.Fl -version
prints the following message and exits (with return code
.Dv EXIT_SUCCESS ) :
.Bd -literal -offset 4m
<program_name> version <version>
.Ed
.Pp
The help page
.Fl ( -help
or
.Fl h )
displays the following message and exits (with return code
.Dv EXIT_SUCCESS ) :
.Bd -literal -offset 4m
<program_name> version <version>
Usage: <usage>
<short_description>

<argument list>
.Ed
.Pp
All of those entries
.Fl ( h , -help
or
.Fl -version )
may be freely redefined by the programmer.
.Pp
When processing command line arguments, the parser may encounter an unknown option or invalid syntax. The library prints the following error message to
.Dv stderr
and exits (with return code
.Dv EXIT_FAILURE ) :
.Bd -literal -offset 4m
Option --<long> (-<short>) <error_message>
Use <program_name> --help to see the list of available options.
.Ed
.Pp
The following errors are handled automatically by the library:
.Bl -diag -offset 4m
.It option specified more than once
- triggered by
.Fn arguments_parse
when an option with the
.Dv ARGUMENT_F_ONCE
flag set is encountered more than once in the command line, see also
.Sx FLAGS ,
.It unknown option
- triggered by
.Fn arguments_parse
if any of the command line arguments is not found in the array,
.It option requires parameter
- triggered by
.Fn arguments_parse
when an option with the
.Dv ARGUMENT_F_PAR
flag set was provided without the required parameter,
.It option does not accept any parameters
- triggered by
.Fn arguments_parse
if the flag
.Dv ARGUMENT_F_PAR
is not set and the command line argument was provided in form 
.Fl -option=... ,
.It option requires integer parameter
- triggered by
.Fn argument_int
when the provided parameter could not be parsed as a number.
.It number being out of range
- triggered by
.Fn argument_int
and
.Fn argument_u
when supplied value is beyond the storing capabilities of their types, which are
.Vt long
and
.Vt unsigned long
respectively.
.El
.Pp
If the programmer himself wants to display an error message, he may do it by 
calling the
.Xr arguments_error 3
function:
.Bd -ragged -offset 4m
.Ft void 
.Fo arguments_error 
.Fa "struct Argument *arg"
.Fa "const char *error_message"
.Fc
.Ed
.Pp
where:
.Bl -hang -offset 4m
.It Fa arg
- option which caused the error, it points at that option, not the whole array passed to
.Fn arguments_parse ,
.It Fa error_message
- additional information (see message syntax above)
.El
.Pp
If one needs to display a bare error message he should use the 
.Xr arguments_error_freeform 3
call:
.Bd -ragged -offset 4m
.Ft void
.Fo arguments_error_freeform 
.Fa "char *format" 
.Fa ...
.Fc
.Ed
.Pp
which bahaves like the
.Xr printf 3
family of functions and displays a short help after the formatted line (see above).
.Sh SEE ALSO
.Xr arguments_parse 3 ,
.Xr arguments_error_freeform 3 ,
.Xr arguments_error 3 ,
.Xr arguments_print_arg 3 ,
.Xr arguments_clr_occurrence 3
.Sh HISTORY
The work on
.Pa libarguments
was started by Matlib in March 2007.
.Sh AUTHORS
.An Matlib Ad matlib@matlibhax.com
