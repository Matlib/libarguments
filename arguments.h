/*
   arguments.h
   
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.

   Change log:
        1  2007-03-05  Matlib
        3  2011-02-15  Matlib
        4  2011-11-19  Matlib  + float target
        5  2013-09-09  Matlib  + argument_syslog
        6  2015-12-25  Matlib  + argument_log
        7  2016-09-05  Matlib (program_invocation_file)
        8  2018-08-04  Matlib  + print option descriptions for --help
        9  2019-03-18  Matlib (fix OS compatility)

*/



#ifndef ARGUMENTS_H

#define ARGUMENTS_H YES

#include <stdio.h>
#include <stdarg.h>

struct Argument;



/*
   Callback procedure type definition
   Use ARGUMENT_CALLBACK to define a callback to remain compatible
   with possible future versions of this library.

   Arguments are:
     * int argc -- same as void main (int argc, char **argv).
     * char **argv -- same as void main (int argc, char **argv).
     * struct Argument *arg -- pointer to the argument structure that 
       matched the command line option; it is not the whole array as 
       passed to arguments_parse!
     * char *parameter -- option's parameter if flag ARGUMENT_F_PAR was 
       set. NULL otherwise.
*/
typedef void ArgumentCallback (int const, char const *const *const,
	 struct Argument *const, char const *const);
#define ARGUMENT_CALLBACK(_NAME_) \
	void _NAME_ (int argc, char const *const *argv, struct Argument *arg, \
		 char const *parameter)



/* struct Argument
     * char shrt - short option (one character), 0 if none.
     * char *lng - long option (string), NULL if none (at least one of
       the two must be present!).
     * Argument_Callback *cb - the procedure that is called every
       time the option is found.
     * int flags - flags, see section Flags.
     * void *value - place where argument callback may store the result,
       also available as unsigned long (value_u), long (value_int),
       const string (value_str), string (value_vs), floating point (value_fp),
       or log callback (value_log).
     * char *parameter_name - the name of the parameter on the --help page.
     * char *description - argument description on the --help page.
*/
struct Argument 
{
	char shrt;
	char const *lng;
	ArgumentCallback *cb;
	int flags;
	union
	{
		void *value;
		unsigned long value_u;
		long value_int;
		char const *value_str;
		char *value_vs;
		#if _LP64 == 1
			double value_fp;
		#else
			float value_fp;
		#endif
		void (*value_log) (int priority, char const *format, va_list ap);
	};
	char const *parameter_name;
	char const *description;
};

/* Flags (struct Argument.flags): */
#define ARGUMENT_F_OCC    0x01    // argument found in command line
#define ARGUMENT_F_PAR    0x02    // argument requires parameter
#define ARGUMENT_F_ONCE   0x04    // argument may occur only once
#define ARGUMENT_F_OBL    0x08    // argument must occur at least once
#define ARGUMENT_F_RET    0x10    // retain OCC flag when clearing



#ifdef __cplusplus
extern "C" {
#endif



/* Standard callbacks */

/* argument_bool increases struct Argument.value by 1. Without flags
   it is verbosity-like option (use -vvvv to increase verbosity). With
   ARGUMENT_F_ONCE it becomes a boolean-type option (on/off)
*/
ARGUMENT_CALLBACK (argument_bool);

/* argument_string points struct Argument.value at argument's parameter.
   Should be used in with flags ARGUMENT_F_PAR | ARGUMENT_F_ONCE to
   make any sense. Should be accessed from value_str.
*/
ARGUMENT_CALLBACK (argument_string);

/* argument_vs (variable string); same as above except that the string
   is duplicated with strdup(3). Should be accessed from value_vs
*/
ARGUMENT_CALLBACK (argument_vs);

/* argument_int parses the integer in parameter and stores the result as
   struct Argument.value_int
*/
ARGUMENT_CALLBACK (argument_int);

/* argument_u parses the integer in parameter and stores the result as
   struct Argument.value_u
*/
ARGUMENT_CALLBACK (argument_u);

/* argument_fp parses the floating point number in parameter which is
   then accessible as struct Argument.value_fp
*/
ARGUMENT_CALLBACK (argument_fp);

/* argument_syslog starts logging facility given in parameter;
   facility code is stored in value_int
*/
ARGUMENT_CALLBACK (argument_syslog);


/* argument_log initializes value_log with the appropriate callback for
   writing to either syslog if parameter is a known facility name, or to 
   stdout/stderr, or to a file
*/
ARGUMENT_CALLBACK (argument_log);
void log_callback_file (int, char const *, va_list ap);




/* arguments_parse - main procedure; parses the command line options and
   executes corresponding callbacks. 

   Parameters are:
     * program_name - your program's name shown in --version.
     * program_version - version, shown in --version.
     * usage - the usage string shows in --help in the form:
       Usage: _program_ [OPTIONS]... _usage_
     * short_description - shown in --help below usage.
     * int argc - as in int main (int argc, char **argv)
     * char **argv - as in int main (int argc, char **argv)
     * struct Argument *arg - the array of available options
*/
int arguments_parse (char const *program_name, char const *program_version, 
	 char const *usage, char const *short_description, int const argc, 
	 char const *const *const argv, struct Argument *const arg);



/* arguments_error_freeform - basic template to print errors,
   exits with rc == EXIT_FAILURE

   Parameters are:
     * char *format - format/template, same as printf,
*/
void arguments_error_freeform (char const *format, ...)
	 __attribute__ ((format (printf, 1, 2)));



/* arguments_error - report an erroneous usage of command line arguments.

   Parameters are:
     * struct Argument *arg - pointer to the argument that caused the
       error, not the whole array as in arguments_parse!
     * char *string - informational string appended to error message in
       the form:
       Argument _arg_ _string_
       Use _program_ --help to see...  */
void arguments_error (struct Argument const *const arg, 
	 char const *const string);



/* arguments_print_arg - print option's identifier

   Parameters are:
     * struct Argument *arg - pointer to the argument to be printed
     * FILE *f - output buffer */
void arguments_print_arg (struct Argument const *const arg, 
	 FILE *const f);




/* arguments_clr_occurrence - clear ARG_F_OCC flag from all options

   Parameters are:
     * struct Argument *arg - the array of arguments */
void arguments_clr_occurrence (struct Argument *const arg);



#ifdef __cplusplus
}
#endif


#endif
