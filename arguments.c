/*
   arguments.c
   
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
     
   Change log:
        1  2007-03-05  Matlib
        2  2009-02-19  Matlib (minor corrections)
        3  2011-02-15  Matlib (union, consts, attr and -O2 fix
                               + arguments_print_par 
                               + arguments_clr_occurrence)
        4  2011-03-23  Matlib (corrections to argument_u)
        5  2013-09-09  Matlib  + arguments_syslog
        6  2015-12-25  Matlib (corrections to printf)
                               + argument_log
        7  2016-09-05  Matlib (program_invocation_file)
        8  2018-08-04  Matlib  + print option descriptions for --help
        9  2019-03-18  Matlib (fix OS compatibility)
                              (word wrapping for description)

*/



#include "arguments.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <time.h>
#include <syslog.h>
#include <termios.h>
#include <strings.h>



#ifndef __USE_GNU
	static char const *program_invocation_short_name;
#else
	static extern char *program_invocation_short_name;
#endif

static char const *program_invocation_file;



static void *malloc_x (size_t const size)
{
	void *ptr;
	if (!(ptr = malloc (size)))
	{
		fprintf (stderr, "%s: libarguments: malloc: %s\n", 
			 program_invocation_short_name, strerror (errno));
		exit (EXIT_FAILURE);
	}
	return ptr;
}

static void *strdup_x (char const *const str)
{
	char *ds;
	if (!(ds = strdup (str)))
	{
		fprintf (stderr, "%s: libarguments: strdup: %s\n", 
			 program_invocation_short_name, strerror (errno));
		exit (EXIT_FAILURE);
	}
	return ds;
}

#define alen(__ARRAY__) \
	 ((unsigned) (sizeof (__ARRAY__) / sizeof ((__ARRAY__) [0])))



int write_fd (int const fd, char const *buffer, size_t buffer_size)
{
	int e;
	while (buffer_size)
	{
		e = write (fd, buffer, buffer_size);
		if (e == -1)
		{
			if (errno == EINTR)
				continue;
			return -1;
		}
		buffer += e;
		buffer_size -= e;
	}
	return 0;
}



void arguments_error_freeform (char const *format, ...)
{
	va_list ap;

	va_start (ap, format);
	vfprintf (stderr, format, ap);
	va_end (ap);
	fprintf (stderr, "\nUse %s --help to see the list of available options.\n",
		 program_invocation_short_name);
	exit (EXIT_FAILURE);
}

void arguments_error (struct Argument const *const arg, 
	 char const *const string)
{
	fprintf (stderr, "Option ");
	arguments_print_arg (arg, stderr);
	arguments_error_freeform (" %s", string);
}

void arguments_print_arg (struct Argument const *const arg, 
	 FILE *const f)
{
	if ((arg -> lng != NULL) && (arg -> shrt) != 0)
		fprintf (f, "--%s (-%c)", arg -> lng, arg -> shrt);
	else if (arg -> lng != NULL)
		fprintf (f, "--%s", arg -> lng);
	else
		fprintf (f, "-%c", arg -> shrt);
}







ARGUMENT_CALLBACK (argument_bool)
{
	arg -> value_u ++;
}

ARGUMENT_CALLBACK (argument_string)
{
	arg -> value_str = parameter;
}

ARGUMENT_CALLBACK (argument_vs)
{
	arg -> value_vs = strdup_x (parameter);
}



/*
   Logging helpers
   System facilitynames available in GNU and FreeBSD are used if SYSLOG_NAMES
   is defined. Otherwise a subset of RFC3164 facility names is supplied. It
   seems to work on systems tested.
*/

#ifndef SYSLOG_NAMES
	struct SyslogFacility
	{
		int const c_val;
		char const *const c_name;
	};
	static struct SyslogFacility const facilitynames [20] =
	{
		{ LOG_KERN,     "kern" },
		{ LOG_USER,     "user" },
		{ LOG_MAIL,     "mail" },
		{ LOG_DAEMON,   "daemon" },
		{ LOG_AUTH,     "auth" },
		{ LOG_SYSLOG,   "syslog" },
		{ LOG_LPR,      "lpr" },
		{ LOG_NEWS,     "news" },
		{ LOG_UUCP,     "uucp" },
		{ LOG_AUTHPRIV, "authpriv" },
		{ LOG_CRON,     "cron" },
		{ LOG_LOCAL0,   "local0" },
		{ LOG_LOCAL1,   "local1" },
		{ LOG_LOCAL2,   "local2" },
		{ LOG_LOCAL3,   "local3" },
		{ LOG_LOCAL4,   "local4" },
		{ LOG_LOCAL5,   "local5" },
		{ LOG_LOCAL6,   "local6" },
		{ LOG_LOCAL7,   "local7" },
		{ 0,            NULL }
	};
#endif

ARGUMENT_CALLBACK (argument_syslog)
{
	unsigned i;
	for (i = 0; facilitynames [i].c_name != NULL; i ++)
		if (!strcasecmp (parameter, facilitynames [i].c_name))
		{
			arg -> value_int = facilitynames [i].c_val;
			return;
		}
	arguments_error (arg, "requires syslog facility name");
}

static int log_fd = STDERR_FILENO;

ARGUMENT_CALLBACK (argument_log)
{
	unsigned i;
	if (!strcasecmp (parameter, "stdout"))
	{
		log_fd = STDOUT_FILENO;
		goto stdset;
	}
	if (!strcasecmp (parameter, "stderr"))
	{
		log_fd = STDERR_FILENO;
stdset:
		arg -> value_log = log_callback_file;
		return;
	}
	for (i = 0; facilitynames [i].c_name != NULL; i ++)
		if (!strcasecmp (parameter, facilitynames [i].c_name))
		{
			openlog (program_invocation_file, LOG_CONS | LOG_NOWAIT | LOG_PID,
				 facilitynames [i].c_val);
			arg -> value_log = vsyslog;
			return;
		}

	if ((log_fd = open (parameter, 
		 O_WRONLY | O_APPEND | O_CREAT | O_NOCTTY, 0640)) == -1)
		arguments_error_freeform ("Cannot create log file '%s': %s.",
			 parameter, strerror (errno));
	arg -> value_log = log_callback_file;
}

void log_callback_file (int const priority, char const *const format, 
	 va_list ap)
{
	int e, p = 0, bs;
	char buffer [1024];
	struct tm tm;
	time_t t = time (NULL);
	if (localtime_r (&t, &tm))
		p = snprintf (buffer, sizeof (buffer), "%d-%02d-%02d %02d:%02d:%02d %s[%d]: ",
			 tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
			 tm.tm_sec, program_invocation_file, getpid ());
	bs = sizeof (buffer) - p;
	va_list ap2;
	va_copy (ap2, ap);
	e = vsnprintf (buffer + p, bs, format, ap2);
	va_end (ap2);
	if (e < bs)
	{
		e += p;
		buffer [e] = '\n';
		for (p = 0; p < e; p ++)
			if (((unsigned char) buffer [p]) < ' ')
				buffer [p] = ' ';
		write_fd (log_fd, buffer, e + 1);
		return;
	}
	char *long_buffer = malloc_x (p + e + 1);
	memcpy (long_buffer, buffer, p);
	e = vsnprintf (long_buffer + p, e + 1, format, ap) + p;
	long_buffer [e] = '\n';
	for (p = 0; p < e; p ++)
		if (((unsigned char) buffer [p]) < ' ')
			buffer [p] = ' ';
	write_fd (log_fd, long_buffer, e + 1);
	free (long_buffer);
}




static const char *INT_RANGE_STR = 
	 "Integer value outside of allowed range for option ";

ARGUMENT_CALLBACK (argument_int)
{
	char *tail;
	arg -> value_int = strtol (parameter, &tail, 0);
	if (*tail != 0)
		arguments_error (arg, "expects integer as parameter.");
	if (errno == ERANGE)
	{
		fputs (INT_RANGE_STR, stderr);
		arguments_print_arg (arg, stderr);
		arguments_error_freeform (".");
	}
}

ARGUMENT_CALLBACK (argument_u)
{
	char *tail;
	long long v;
	v = strtoll (parameter, &tail, 0);
	if ((*tail != 0) || (v < 0))
		arguments_error (arg, "expects positive integer as parameter.");
	if (v > ULONG_MAX)
	{
		fputs (INT_RANGE_STR, stderr);
		arguments_print_arg (arg, stderr);
		arguments_error_freeform (".");
	}
	arg -> value_u = v;
}



ARGUMENT_CALLBACK (argument_fp)
{
	char *tail;
	errno = 0;
	#if _LP64 == 1
		double v;
		v = strtod (parameter, &tail);
	#else
		float v;
		v = strtof (parameter, &tail);
	#endif
	if (*tail)
		arguments_error (arg, "expects a number as parameter.");
	if (errno || !isfinite (v))
	{
		fprintf (stderr, "Number value is outside of allowed range for option ");
		arguments_print_arg (arg, stderr);
		arguments_error_freeform (".");
	}
	arg -> value_fp = v;
}






static void break_lines (unsigned const width, unsigned const indent,
	 unsigned w, char const *p)
{
	char const *i, *e;
	size_t ww;
	char const *spacer = "";
	if (p)
	{
		printf ("%*s", indent - w, "");
		w = indent;
		e = p + strlen (p);
		while (p < e)
		{
			if ((i = strpbrk (p, " \n")))
				ww = (size_t) i - (size_t) p;
			else
				ww = strlen (p);
			w += ww + 1;
			if (w > width)
			{
				printf ("\n%*s", indent, "");
				w = indent + ww + 1;
				spacer = "";
			}
			printf ("%s%.*s", spacer, (int) ww, p);
			if (!i)
				break;
			p = i + 1;
			if (*i == '\n')
			{
				printf ("\n%*s", indent, "");
				w = indent;
				spacer = "";
			}
			else
				spacer = " ";
		}
	}
	putchar ('\n');
}

void arguments_version (char const *const program_name, 
	 char const *const program_version)
{
	if (program_name != NULL)
	{
		fputs (program_name, stdout);
		if (program_version != NULL)
			printf (" version %s", program_version);
		putchar ('\n');
	}
}

void arguments_help_print_arg (struct Argument const *const arg, int *const f,
	 unsigned const width, unsigned width_opt)
{
	unsigned w = 4;
	width_opt += 5;

	if (arg -> shrt)
	{
		printf ("  -%c", (*arg).shrt);
		if (arg -> shrt == 'h')
			*f |= 1; // flags = 1 (-h) | 2 (--help) | 4 (--version)
	}
	else
		printf ("    ");

	if (arg -> lng)
	{
		printf ("  --%s", arg -> lng);
		w += 4 + strlen (arg -> lng);
		if (strcmp (arg -> lng, "help") == 0)
			*f |= 2; // flags again, see note above
		else if (strcmp (arg -> lng, "version") == 0)
			*f |= 4;
	}

	if (arg -> parameter_name)
	{
		printf ("%c%s", arg -> lng ? '=' : ' ', arg -> parameter_name);
		w += 1 + strlen (arg -> parameter_name);
	}
	else if (arg -> flags & ARGUMENT_F_PAR)
	{
		printf ("%cPAR", arg -> lng ? '=' : ' ');
		w += 4;
	}

	break_lines (width, width_opt + 1, w, arg -> description);
}

void arguments_help (char const *const *const argv, 
	 struct Argument const *const arg, 
	 char const *const program_name, char const *const program_version, 
	 char const *const usage, char const *const description)
{
	int i, f, nl;
	arguments_version (program_name, program_version);

	unsigned width = 79, width_opt = 7;
	struct winsize ws;
	if (!ioctl (STDOUT_FILENO, TIOCGWINSZ, &ws) && (ws.ws_col > 0))
		width = ws.ws_col - 1;
	if (arg)
		for (i = 0; arg [i].cb; i ++)
		{
			nl = 0;
			if (arg [i].lng)
				nl += strlen (arg [i].lng) + 4;
			if (arg [i].parameter_name)
				nl += strlen (arg [i].parameter_name) + 1;
			else if (arg [i].flags & ARGUMENT_F_PAR)
				nl += 4;
			if (nl > width_opt)
				width_opt = nl;
		}

	f = 0;
	printf ("\nUsage: %s [OPTION]... ", argv [0]);
	if (usage)
		fputs (usage, stdout);
	putchar ('\n');
	if (description)
	{
		fputc ('\n', stdout);
		break_lines (width, 0, 0, description);
		fputc ('\n', stdout);
	}

	nl = 0;
	if (arg)
		for (i = 0; arg [i].cb; i ++)
			if ((arg [i].flags & ARGUMENT_F_OBL) != 0)
			{
				if (nl == 0)
				{
					putchar ('\n');
					nl ++;
				}
				arguments_help_print_arg (&(arg [i]), &f, width, width_opt);
			}
	printf ("\nOptional arguments:\n\n");
	if (arg)
		for (i = 0; arg [i].cb; i ++)
			if ((arg [i].flags & ARGUMENT_F_OBL) == 0)
				arguments_help_print_arg (&(arg [i]), &f, width, width_opt);

	/* print --help and --version if not redefined */
	if (f != 7)
	{
		putchar ('\n');
		printf ((f & 1) == 0 ? "  -h" : "    ");
		if ((f & 2) == 0)
			printf ("  --help");
		putchar ('\n');
		if ((f & 4) == 0)
			printf ("      --version\n");
	}

	putchar ('\n');   
	exit (EXIT_SUCCESS);
}





static void arguments_mark_option (char const *const *const argv, 
   struct Argument *const arg)
{
	if ((arg -> flags & (ARGUMENT_F_OCC | ARGUMENT_F_ONCE)) 
		 == (ARGUMENT_F_OCC | ARGUMENT_F_ONCE))
	arguments_error (arg, "specified more than once.");
	arg -> flags |= ARGUMENT_F_OCC;
}

int arguments_parse (char const *const program_name, 
	 char const *const program_version, char const *const usage, 
	 char const *const short_description, int const argc, 
	 char const *const *const argv, struct Argument *const arg)
{
	int a, i, pos;
	#ifndef __USE_GNU
		if (!program_invocation_short_name)
			program_invocation_short_name = argv [0];
	#endif
	if ((program_invocation_file = rindex (program_invocation_short_name, '/')))
		program_invocation_file ++;
	else
		program_invocation_file = program_invocation_short_name;
	for (a = 1; (a < argc) && (strlen (argv [a]) > 1) && 
		 (argv [a][0] == '-'); a ++)
	{
		i = 0;
		if (argv [a][1] == '-')     // --long options
		{
			if (argv [a][2] == 0)     // stop processing at --
			{
				a ++;
				break;
			}
			for (pos = 0; (argv [a][pos] != 0) && (argv [a][pos] != '='); pos ++);
lloop:
			if ((arg == NULL) || (arg [i].cb == NULL))
			{
				if (strcmp (argv [a], "--version") == 0)
				{
					arguments_version (program_name, program_version);
					exit (EXIT_SUCCESS);
				}
				if (strcmp (argv [a], "--help") == 0)
					arguments_help (argv, arg, program_name, program_version, usage, 
						 short_description);
				arguments_error_freeform ("Unknown option: %s.", argv [a]);
			}
			if ((arg [i].lng == NULL) 
				 || (strncmp (arg [i].lng, argv [a] + 2, pos - 2  ) != 0))
			{
				i ++;
				goto lloop;
			}
			arguments_mark_option (argv, &(arg [i]));
			if (argv [a][pos] == '=')
			{
				pos ++;
				if ((arg [i].flags & ARGUMENT_F_PAR) == 0)
					arguments_error (&(arg [i]), "does not accept any parameters.");
			}
			if ((arg [i].flags & ARGUMENT_F_PAR) == 0)
				arg [i].cb (argc, argv, arg + i, NULL);
		}
		else                        // -s hort options
		{
			pos = 1;
sloop:
			if ((arg == NULL) || (arg [i].cb == NULL))
			{
				if (argv [a][pos] == 'h')
					arguments_help (argv, arg, program_name, program_version, usage, 
						 short_description);
				else
					arguments_error_freeform ("Unknown option: -%c.", argv [a][pos]);
			}
			if (arg [i].shrt != argv [a][pos])
			{
				i ++;
				goto sloop;
			}
			arguments_mark_option (argv, arg + i);
			pos ++;
			if ((arg [i].flags & ARGUMENT_F_PAR) == 0)
			{
				arg [i].cb (argc, argv, arg + i, NULL);
				i = 0;
				if (argv [a][pos] != 0)
					goto sloop;
				continue;
			}
		}

		if ((arg [i].flags & ARGUMENT_F_PAR) != 0)
		{
			if (argv [a][pos] == 0)
			{
				a ++;
				pos = 0;
				if (a >= argc)
					arguments_error (&(arg [i]), "requires parameter.");
			}
			arg [i].cb (argc, argv, arg + i, argv [a] + pos);
		}
	}


	// Checking which obligatory arguments were not found
	if (arg != NULL)
		for (i = 0; arg [i].cb != NULL; i ++)
			if (((arg [i].flags & ARGUMENT_F_OBL) != 0) 
				 && ((arg [i].flags & ARGUMENT_F_OCC) == 0))
				arguments_error (&(arg [i]), "must be specified.");

	return a;
}



void arguments_clr_occurrence (struct Argument *const arg)
{
	unsigned i;
	for (i = 0; arg [i].cb != NULL; i ++)
		if ((arg [i].flags & ARGUMENT_F_RET) == 0)
			arg [i].flags &= ~ARGUMENT_F_OCC;
}

