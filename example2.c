/*
   gcc -oexample2 example2.c -larguments
*/

#include <stdio.h>
#include <arguments.h>

struct Argument arguments [] =
{
  {'v', "verbose", argument_bool},
  {0,   "string",  argument_string, ARGUMENT_F_ONCE |
    ARGUMENT_F_PAR, {.value_str = "(default value)"}},
  {}
};

int main (const int argc, const char **argv)
{
  int p;
  p = arguments_parse ("example", "1.0", "[OTHER]",
    "Demo", argc, argv, arguments);
  printf ("Verbosity level is: %ld; string = %s\n",
    arguments [0].value_int, arguments [1].value_str);
  if (p < argc)
    printf ("First non-option argument: %s\n",
      argv [p]);
  return 0;
}

