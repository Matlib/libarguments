/*
   gcc -oexample1 example1.c -larguments
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arguments.h>

static ARGUMENT_CALLBACK (argument_data)
{
	if (strlen (parameter) < 6)
		arguments_error (arg,
			 "requires a parameter with at least 6 characters.");
	arg -> value_str = parameter;
}

static const float PT = 1.;
static const float IN = 72.;
static const float MM = 2.8346457;

static ARGUMENT_CALLBACK (argument_unit)
{
	if (strcasecmp (parameter, "pt") == 0)
		arg -> value_fp = PT;
	else if (strcasecmp (parameter, "in") == 0)
		arg -> value_fp = IN;
	else if (strcasecmp (parameter, "mm") == 0)
		arg -> value_fp = MM;
	else
		arguments_error_freeform ("Invalid unit of measure given (%s).", 
			 parameter);
}

struct Argument arguments [] =
{
	{
		'f', "foo",
		argument_data,
		ARGUMENT_F_ONCE | ARGUMENT_F_PAR,
		{.value = NULL},
		"FOODATA",
		"set the foo text\n"
		"the initial value is simply 'none'"
	},
	{
		'b', "bar",
		argument_bool,
		ARGUMENT_F_ONCE,
		{.value_u = 0},
		NULL,
		"set the bar option\n"
		"this is a flag only, specifying this option will cause 'yes' to be printed"
	},
  {
		'h', "hops",
		argument_int,
		ARGUMENT_F_ONCE | ARGUMENT_F_PAR,
		{.value_int = 0},
		"INTEGER",
		"the hops option accepts a simple integer"
	},
	{
		's', "scale",
		argument_unit,
		ARGUMENT_F_PAR,
		{.value = NULL},
		"UNIT",
		"sets a custom scale parameter to the given unit\n"
		"this can be 'pt', 'in', or 'mm' and the actual float value is printed "
		"as the result"
	},
  {
		'v', "verbose",
		argument_bool,
		0,
		{.value_u = 3} // default verbosity level is 3
	},
  {
		'q', "quiet",
		argument_bool,
		0,
		{.value_u = 0}
	},
	{}
};

enum
{
	ARG_N_FOO, ARG_N_BAR, ARG_N_HOPS, ARG_N_SCLE, ARG_N_VERB, ARG_N_QIET
};

#define ARG_FOO  (arguments [ARG_N_FOO].value_str)
#define ARG_BAR  (arguments [ARG_N_BAR].value_u)
#define ARG_HOPS (arguments [ARG_N_HOPS].value_int)
#define ARG_SCLE (arguments [ARG_N_SCLE].value_fp)
#define ARG_VERB (arguments [ARG_N_VERB].value_u)
#define ARG_QIET (arguments [ARG_N_QIET].value_u)

int main (const int argc, const char **argv)
{
	int p;
	p = 0;
	while (p < argc - 1)
	{
		ARG_FOO = "none";
		ARG_BAR = 0;
		ARG_HOPS = 0;
		p += arguments_parse ("foo", "1.0", "string ...",
			 "Fooz the filez!", argc - p, argv + p, arguments);
		if (p == argc)
			arguments_error_freeform ("String is required.");
		printf ("Processing \"%s\"\n  verbosity level is %ld\n  foo=\"%s\""
			 "\n  bar=%s\n  hops=%ld\n  scale=%f\n\n", argv [p], 
			 (long) ARG_VERB - (long) ARG_QIET, ARG_FOO, ARG_BAR == 0 ? "no" : "yes",
			 ARG_HOPS, ARG_SCLE);
		arguments_clr_occurrence (arguments);
	}
	return EXIT_SUCCESS;
}
