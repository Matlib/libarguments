/*
   gcc -oexample3 example3.c -larguments
*/

#include <stdarg.h>
#include <syslog.h>

#include <arguments.h>

struct Argument arguments [] =
{
	{
		'l',
		"log",
		argument_log,
		ARGUMENT_F_ONCE | ARGUMENT_F_PAR,
		{.value_log = log_callback_file},
		"destination",
		"Logging destination. Can be one of syslog's facilities, stdout, stderr, "
		"or a file name."
	},
	{
		'p',
		"priority",
		argument_int,
		ARGUMENT_F_ONCE | ARGUMENT_F_PAR,
		{.value_int = LOG_NOTICE},
		"integer",
		"Logging priority sent to syslog (0-7). Lower values indicate higher "
		"message priority. Defaults to 'notice' (5)."
	},
	{ }
};

enum
{
	ARGN_LOG, ARGN_PRIO
};

#define PRIO (arguments [ARGN_PRIO].value_int)

void wlog (char const *const format, ...)
{
	va_list ap;
	va_start (ap, format);
	arguments [ARGN_LOG].value_log (PRIO, format, ap);
	va_end (ap);
}

int main (const int argc, const char **argv)
{
	int p;
	p = arguments_parse ("example3", "1.0", "text...",
		"Logging example - texts given in command line are written to the desired "
		"log file or syslog facility. The output destination is controlled by the "
		"--log parameter and defaults to standard error.", argc, argv, arguments);
	while (p < argc)
		wlog ("%s", argv [p ++]);
	return 0;
}
